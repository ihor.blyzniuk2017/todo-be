<?php

namespace App\Http\Controllers;

use App\Models\Director;
use App\Models\Film;
use Illuminate\Http\Request;
use App\Http\Resources\FilmResource;

class FilmsController extends Controller
{
    public function index()
    {
        $films = FilmResource::collection(Film::all());
        return response()->json($films);
    }
}
