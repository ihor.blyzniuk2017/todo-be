<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        try {
            $user = User::create($request->all());
            $token = $user->createToken('');

            return response()->json($token);
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Could not register user.',
                'exception' => $ex->getMessage()
            ], 500);
        }
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        try {
            if (Auth::attempt($credentials)) {
                $request->session()->regenerate();
                $token = $request->user()->createToken('');

                return response()->json($token);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Could not login user.',
                'exception' => $ex->getMessage()
            ], 500);
        }
    }

    public function logout(Request $request)
    {

    }
}
