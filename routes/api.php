<?php

use App\Http\Controllers\CinemasController;
use App\Http\Controllers\MoviesController;
use App\Http\Controllers\OfdbController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AuthorsController;
use App\Http\Controllers\BooksController;
use App\Http\Controllers\FilmsController;
use App\Http\Controllers\TodosController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('/register', [AuthController::class, 'register']);

Route::post('/login', [AuthController::class, 'login']);

Route::group(['prefix' => '/todos'], function() {
    Route::get('/', [TodosController::class, 'index']);
    Route::post('/', [TodosController::class, 'store']);
    Route::post('{id}', [TodosController::class, 'edit']);
    Route::delete('{id}', [TodosController::class, 'destroy']);
});

Route::get('/films', [FilmsController::class, 'index']);

Route::get('/books', [BooksController::class, 'index']);

Route::get('/authors', [AuthorsController::class, 'index']);

Route::get('/cinemas', [CinemasController::class, 'index']);
Route::get('/movies', [MoviesController::class, 'index']);

//'localhost::8000/ofdb/{url}'
Route::get('/ofdb/{params?}/{param2?}', [OfdbController::class, 'index']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
