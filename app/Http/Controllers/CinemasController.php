<?php

namespace App\Http\Controllers;

use App\Http\Resources\BookResource;
use App\Http\Resources\CinemaResource;
use App\Http\Resources\FilmResource;
use App\Models\Book;
use App\Models\Cinema;
use Illuminate\Http\Request;

class CinemasController extends Controller
{
    public function index()
    {
        $cinemas = CinemaResource::collection(Cinema::all());
        return response()->json($cinemas);
    }
}
