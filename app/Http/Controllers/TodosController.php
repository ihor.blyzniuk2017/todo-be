<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;

class TodosController extends Controller
{
    public function index()
    {
        try {
            $todos = Todo::all();
            return response()->json(['data' => $todos], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Could not get todos',
                'exception' => $ex->getMessage()
            ], 500);
        }

    }

    public function store(Request $request)
    {
        try {
            $title = $request->input('title');
            $todo = Todo::create(['title' => $title]);
            $todos = Todo::all();
            if($todo) {;
                return response()->json(['data' => $todos], 200);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Could not create todo',
                'exception' => $ex->getMessage()
            ], 500);
        }
    }

    public function edit(Request $request, $id)
    {
        try {
            $title = $request->input('title');
            $todo = Todo::findOrFail($id);
            $todo->update(['title' => $title]);
            $todos = Todo::all();
            if($todo) {
                return response()->json(['data' => $todos], 200);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Could not edit todo',
                'exception' => $ex->getMessage()
            ], 500);
        }

    }

    public function destroy(Request $request, $id)
    {
        try {
            $todo = Todo::findOrFail($id);
            $todo->delete();
            $todos = Todo::all();

            return response()->json(['data' => $todos], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'message' => 'Could not delete todo',
                'exception' => $ex->getMessage()
            ], 500);
        }

    }
}
